import React, { useEffect } from 'react'
import { NavigationContainer } from "@react-navigation/native";
import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
  } from '@react-navigation/drawer';

function CustomDrawer(props) {
    useEffect(() => {
        props.chainNavObject(props.navigation)
    }, [])
    return (
        <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            <DrawerItem
            label="Help"
            onPress={() => props.navigation.navigate("HomeStack", {screen: "List"})}
            />
        </DrawerContentScrollView>
    );
}

export default CustomDrawer