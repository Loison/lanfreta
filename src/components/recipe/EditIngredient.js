import React, { useEffect, useState } from 'react'
import { Text, Image, StyleSheet, View, TextInput } from 'react-native'
import { Picker } from '@react-native-community/picker'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import Fuse from 'fuse.js'

function EditIngredient({ingredients, units}) {
    const [unit, setUnit] = useState(null)
    const [ingredient, setIngredient] = useState(null)
    const [ingredientSuggestions, setIngredientSuggestions] = useState([])
    const [ingredientSearchText, setIngredientSearchText] = useState('')
    
    const handleChangeUnit = (itemValue, itemIndex) => {
        setUnit(itemValue)
    }

    const handleIngredientTextChange = (text) => {
        setIngredientSearchText(text)
        text = text.trim()
        if(text.length === 0) {
            setIngredientSuggestions([])
        } else {
            const options = {
                useExtendedSearch: true
            }
            const fuse = new Fuse(ingredients, options)
            const result = fuse.search(`'${text}`, {limit: 20})
            setIngredientSuggestions(result)
        }
    }

    const chooseIngredient = (text) => {
        setIngredient(text)
        setIngredientSearchText(text)
        setIngredientSuggestions([])
    }
    
    return (
        <>
            {units && ingredients &&
            <View style={styles.container}>
                {/* QUANTITY     */}
                <View style={[styles.field, styles.qt]}>
                    <TextInput
                    textAlign="center"
                    keyboardType="number-pad"
                    placeholder={'0'} />
                </View>
                {/* UNIT */}
                <View style={[styles.field, styles.unit]}>
                    {unit != 'other' &&
                    <Picker
                    style={styles.picker}
                    selectedValue={unit}
                    onValueChange={handleChangeUnit}
                    mode='dropdown'>
                        <Picker.Item label='Unité' value={null} key='none' />
                        {units.map((v) => <Picker.Item label={v} value={v} key={v} />)}
                        <Picker.Item label='Autre' value='other' key='other' />
                    </Picker>}
                    {unit == 'other' &&
                    <TextInput placeholder='Unité' />}
                </View>
                {/* NAME */}
                <View style={styles.name}>
                    <TextInput placeholder="Ingredient"
                    value={ingredientSearchText}
                    autoFocus={true}
                    style={[styles.field, styles.input]}
                    onFocus={() => handleIngredientTextChange(ingredientSearchText)}
                    onChangeText={text => handleIngredientTextChange(text)}
                    onSubmitEditing={e => chooseIngredient(ingredientSearchText)}
                    onBlur={e => chooseIngredient(ingredientSearchText)} 
                    />
                    <FlatList
                    style={{backgroundColor: 'green'}}
                    data={ingredientSuggestions}
                    keyExtractor={i => i.refIndex.toString()}
                    renderItem={({item}) => (
                        <TouchableOpacity onPress={() => chooseIngredient(item.item)}>
                            <Text>{item.item}</Text>
                        </TouchableOpacity>
                    )}
                    // ItemSeparatorComponent={ItemSeparator}
                    keyboardShouldPersistTaps='handled' />
                </View>
            </View>}
            {(!units || !ingredients) &&
            <View style={styles.center}>
                <Image source={require('../../assets/images/loading.gif')} style={styles.loading} />
            </View>}
        </>
    )
}

const styles = StyleSheet.create({
    loading: {
        width: 50,
        height: 50
    },
    center: {
        alignItems: 'center'
    },
    container: {
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    picker: {
        flexGrow: 1
    },
    field: {
        height: 50
    },
    name: {
        flexGrow: 1
    },
    unit: {
        flexGrow: 1,
        flexBasis: 150,
        maxWidth: 150,
        backgroundColor: 'pink'
    },
    qt: {
        flexBasis: 50,
        maxWidth: 50,
        flexGrow: 1,
        backgroundColor: 'deeppink'
    }
})

export default EditIngredient;