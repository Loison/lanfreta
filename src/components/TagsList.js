import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Text, TouchableOpacity, FlatList, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { removeFromArray } from '../utils'

function TagsList(props) {
    const [tags, setTags] = useState([])
    const [inputVisible, setInputVisible] = useState(false)
    const [inputText, setInputText] = useState('')

    var toggleInput = () => {
        setInputVisible(!inputVisible)
    }
    
    var handleTextChange = (text) => {
        setInputText(text)
        props.onChangeText(text)
    }

    var addTag = (tag) => {
        if(tags.includes(tag)) return
        let newTags = [...tags]
        newTags.push(tag)
        setTags(newTags)
        props.onchangeTags(newTags)
        // clear and hide input
        handleTextChange('')
        toggleInput()
    }

    var removeTag = (tag) => {
        let newTags = [...tags]
        newTags = removeFromArray(newTags, tag)
        setTags(newTags)
        props.onchangeTags(newTags)
    }

    return (
        <>
            <View style={styles.container}>
                <View style={styles.tags}>
                    <Text>{ props.title } : </Text>
                    {tags.map((value, index) => {
                        return (
                            <View style={styles.tag} key={index}>
                                <Text style={styles.tagText}>{value}</Text>
                                <TouchableOpacity style={styles.delete} onPress={(e) => {
                                    removeTag(value)
                                }}>
                                    <Icon name="times" size={13} />
                                </TouchableOpacity>
                            </View>
                        )
                    })}
                    {!inputVisible && 
                    <TouchableOpacity style={styles.addContainer} onPress={toggleInput}>
                        <View style={styles.add}>
                            <Icon name="plus" size={15} />
                        </View>
                        <Text>Apondre</Text>
                    </TouchableOpacity>}
                </View>
            </View>
            
            {inputVisible &&
            <View style={styles.inputContainer}>
                <View style={styles.inputZone}>
                    <TextInput style={styles.input} onChangeText={text => handleTextChange(text)} clearButtonMode="always" onSubmitEditing={e => addTag(inputText)} autoFocus={true} />
                    <FlatList
                    data={props.suggestions}
                    // extraData={resultList}
                    keyExtractor={i => i[props.idField]}
                    renderItem={({item}) => (
                        <TouchableOpacity onPress={e => {
                            addTag(item[props.valueField])

                        }} >
                            <Text>{item[props.valueField]}</Text>
                        </TouchableOpacity>
                    )}
                    ListEmptyComponent={<Text>Cap d'ingredient correspondent</Text>}
                    keyboardShouldPersistTaps='handled' />
                </View>
                <TouchableOpacity onPress={toggleInput}>
                    <Icon name="times" size={25} />
                </TouchableOpacity>
            </View>}
        </>
    )
}

TagsList.defaultProps = {
    title: 'Tags',
    valueField: 'value',
    idField: 'id'
}

const styles = StyleSheet.create({
    container: {},
    tags: {
        flexDirection: 'row',
        flexShrink: 1,
        flexWrap: 'wrap',
    },
    tag: {
        flexDirection: 'row',
        alignItems: 'stretch',
        backgroundColor: 'lightgrey',
        borderRadius: 5,
        marginHorizontal: 5,
        marginBottom: 5,
        overflow: 'hidden'
    },
    tagText: {
        textAlignVertical: 'center',
        marginHorizontal: 5
    },
    delete: {
        justifyContent: 'center',
        backgroundColor: 'silver',
        paddingHorizontal: 3,
    },
    addContainer: {
        flexDirection: 'row',
        marginLeft: 5
    },
    add: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 50,
        width: 20,
        height: 20,
        marginRight: 5
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    inputZone: {
        flexGrow: 1
    },
    input: {
        borderWidth: 1,
        borderColor: 'black',
        paddingVertical: 0,
        width: 150,
        borderRadius: 5
    }
})

export default TagsList