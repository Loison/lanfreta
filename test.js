const { getRecipe, parseRecipe } = require('./src/parse-recipe');

// (async () => {
//     let recipe = await getRecipe('https://www.marmiton.org/recettes/recette_crepes-au-sarrasin-farcies-a-l-oeuf-fromage-et-jambon_71106.aspx')
//     if(!recipe) return
//     console.log(parseRecipe(recipe))
// })()

let crypto;
try {
  crypto = require('crypto');
} catch (err) {
  console.log('crypto support is disabled!');
}

console.log(crypto.createHash('md5').update("Milon").digest('hex'))