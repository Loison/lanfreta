import React, { useEffect, useState } from 'react'
import { Text, StyleSheet, FlatList, TextInput, View, TouchableWithoutFeedback, Button } from 'react-native'
import PouchDB from '../../pouchdb'
import { localDB, remoteDB } from '../../utils'
import RecipeTile from '../recipe/RecipeTile'
import Icon from 'react-native-vector-icons/FontAwesome'
import TagsList from '../TagsList'
import Fuse from 'fuse.js'

var handlerSync = null

function Search({route, navigation}) {
    const [loading, setLoading] = useState(false)
    const [query, setQuery] = useState(null)
    const [searchResults, setSearchResults] = useState(null)
    const [showAdvanced, setShowAdvanced] = useState(false)
    // Filter Ingredients
    const [ingredients, setIngredients] = useState([])
    const [ingredientsSuggestions, setIngredientsSuggestions] = useState([])
    const [ingredientsFilter, setIngredientsFilter] = useState([])
    // Filter Tags
    const [tags, setTags] = useState([])
    const [tagsSuggestions, setTagsSuggestions] = useState([])
    const [tagsFilter, setTagsFilter] = useState([])

    var highlightResults = (results) => {
        results.forEach(element => {
            element['key'] = element.id
            element.doc.text = element.doc.name
            if(element.highlighting && "name" in element.highlighting) {
                element.doc.text = element.highlighting.name.split(/<\/?strong>/).map((val, index) => {
                    if(index % 2 !== 0) {
                        return (<Text key={element.id + index} style={styles.bold}>{val}</Text>)
                    } else {
                        return (<Text key={element.id + index}>{val}</Text>)
                    }
                })
            }
        })
        return results
    }
    var syncDb = () => {
        handlerSync = PouchDB.sync(remoteDB, localDB, {
            live: true,
            retry: true,
        })
        .on('change', (info) => {
        })
        .on('paused', (err) => {
        })
        .on('active', () => {
        })
        .on('denied', (err) => {
        })
        .on('complete', (info) => {
        })
        .on('error', (err) => {
        })
    }
    var performSearch = (query) => {
        query = query.trim()
        setQuery(query)
        setLoading(true)
        
        if(query == '' && ingredientsFilter.length + tagsFilter.length === 0) {
            setQuery(null)
            setLoading(false)
            setSearchResults(null)
            return
        }
        
        var runQuery = true,
            tagsOnly = false
        
        var searchFunction = () => {
            // Use pouchdb-quick-search for normal query or ingredients query
            if(query != '' || ingredientsFilter.length > 0) {
                var fields, mm
                if(query != '') {
                    fields = {
                        'name': 20,
                        '_id': 10, // pb d' réglé
                        'recipeIngredient.name': 5,
                        'recipeIngredient.ingredient.name': 5,
                        'tags': 10,
                        'recipeInstructions.text': 1
                    }
                    mm = 100
                } else { // (ingredientsFilter.length > 0)
                    query = ingredientsFilter.join(' ')
                    fields = ['recipeIngredient.name', 'recipeIngredient.ingredient.name']
                    mm = 1 / query.split(' ').length * 100
                }
                return localDB.search({
                    query,
                    fields,
                    mm: 100,
                    highlighting: true,
                    include_docs: true
                })
                // Use Mango query for tags only search
            } else { // (tagsFilter.length > 0)
                tagsOnly = true
                return localDB.find({
                    selector: {
                        "tags": {
                            "$in": tagsFilter
                        }
                    },
                    include_docs: true
                })
            }
        }
        
        searchFunction().then(function (res) {
            var list
            if(tagsOnly) {
                list = res.docs.map(d => ({doc: d, id: d._id}))
            }  else {
                list = res.rows
                // ingredients filter
                if(query != '' && ingredientsFilter.length > 0) {
                    const options = {
                        useExtendedSearch: true,
                        keys: ['doc.recipeIngredient.name', 'doc.recipeIngredient.ingredient.name'],
                        includeScore: true,
                        threshold: 0.3
                    }
                    const fuse = new Fuse(res.rows, options)
                    const result = fuse.search(ingredientsFilter.join('|'))
                    list = result.map(r => r.item)
                }
                // tags filter
                if(tagsFilter.length > 0) {
                    list = list.filter((value, index, array) => {
                        if(!value.doc.tags) return false
                        return tags.every(t => value.doc.tags.includes(t))
                    })
                }
            }
            setSearchResults(highlightResults(list))
            setLoading(false)
        }).catch(function (err) {
            console.log('Quick search error : ', err)
        })
    }

    /*****************/
    /*  INGREDIENTS  */
    /*****************/

    var getIngredients = () => {
        localDB.query('ingredients_index', {group: true}).then(function (result) {
            setIngredients(result.rows.map(r => r.key))
        }).catch(function (err) {
            console.log(err)
        })
    }

    var handleIngredientsTextChange = (text) => {
        text = text.trim()
        if(ingredients.length === 0) return
        if(text.length === 0) {
            setIngredientsSuggestions([])
        } else {
            const options = {
                useExtendedSearch: true
            }
            const fuse = new Fuse(ingredients, options)
            const result = fuse.search(`'${text}`)
            setIngredientsSuggestions(result)
        }
    }

    var handleIngredientsTagsChange = (tags) => {
        setIngredientsFilter(tags)
    }

    /**********/
    /*  TAGS  */
    /**********/

    var getTags = () => {
        localDB.query('tags_index', {group: true}).then(function (result) {
            setTags(result.rows.map(r => r.key))
        }).catch(function (err) {
            console.log(err)
        })
    }

    var handleTagsTextChange = (text) => {
        text = text.trim()
        if(tags.length === 0) return
        if(text.length === 0) {
            setTagsSuggestions([])
        } else {
            const options = {}
            const fuse = new Fuse(tags, options)
            const result = fuse.search(text)
            setTagsSuggestions(result)
        }
    }

    var handleTagsTagsChange = (tags) => {
        setTagsFilter(tags)
    }

    
    var handleResultPress = (id) => {
        navigation.navigate('Recipe', {id})
    }
    var toggleAdvanced = () => {
        setShowAdvanced(!showAdvanced)
    }

    useEffect(() => {
        syncDb()
        if(route?.params?.query) {
            performSearch(route.params.query)
        }
        if(ingredients.length === 0) {
            getIngredients()
        }
        if(tags.length === 0) {
            getTags()
        }
        return () => {
            if(handlerSync !== null) handlerSync.cancel()
        }
    }, [route?.params?.query])
    
    return (
        <>
            <TextInput
            defaultValue={route?.params?.query || ''}
            placeholder="Cercar una recèpta, un ingredient, ..."
            onChangeText={t => setQuery(t)}
            onSubmitEditing={_ => performSearch(query)}
            returnKeyType="search" />
            <TouchableWithoutFeedback onPress={toggleAdvanced}>
                <View style={styles.advancedTitle}>
                    <Text>Recherche avancée</Text>
                    <Icon name={showAdvanced ? 'chevron-up' : 'chevron-down'} size={15} />
                </View>
            </TouchableWithoutFeedback>
            <View style={{height: showAdvanced ? 'auto' : 0, overflow: "hidden", margin: 15}}>
                <TagsList
                onChangeText={handleIngredientsTextChange}
                onchangeTags={handleIngredientsTagsChange}
                suggestions={ingredientsSuggestions}
                valueField="item"
                idField="refIndex"
                title="Ingredients" />
                <Text></Text>
                <TagsList
                onChangeText={handleTagsTextChange}
                onchangeTags={handleTagsTagsChange}
                suggestions={tagsSuggestions}
                valueField="item"
                idField="refIndex"
                title="Tags" />
            </View>
            <View style={styles.buttonContainer}>
                <Button title="Lançar la recèrca" onPress={_ => performSearch(query ? query : '')} />
            </View>
            {loading &&
            <Text>Cargament...</Text>}
            {!loading && searchResults?.length > 0 &&
            <>
                {query != '' && <Text style={styles.title}>Résultats pour '{ query }'</Text>}
                <FlatList
                data={searchResults}
                renderItem={({item}) => <RecipeTile recipe={item.doc} onPress={handleResultPress} />} />
            </>
            }
            {!loading && searchResults?.length == 0 &&
            <Text>Pas cap de resultat</Text>}
        </>
    )
}

const styles = StyleSheet.create({
    bold: {
        fontWeight: "bold"
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 20
    },
    advancedTitle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 15
    }
})

export default Search