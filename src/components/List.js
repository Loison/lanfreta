import React, { useState, useEffect } from 'react'
import PouchDB from '../pouchdb'
import { remoteDB, localDB } from '../utils'
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    ToastAndroid,
    FlatList,
    Button
} from 'react-native'

let handlerSync = null

function List({ navigation }) {
    const [error, setError] = useState('')
    const [receptas, setReceptas] = useState(null)
    var syncDb = () => {
        handlerSync = PouchDB.sync(remoteDB, localDB, {
            live: true,
            retry: true,
        })
        .on('change', (info) => {
            ToastAndroid.show('change ' + info, ToastAndroid.LONG)
        })
        .on('paused', (err) => {
            ToastAndroid.show('paused ' + err, ToastAndroid.LONG)
        })
        .on('active', () => {
            ToastAndroid.show('activate', ToastAndroid.LONG)
        })
        .on('denied', (err) => {
            ToastAndroid.show('denied ' + err, ToastAndroid.LONG)
        })
        .on('complete', (info) => {
            ToastAndroid.show('complete ' + info, ToastAndroid.LONG)
        })
        .on('error', (err) => {
            ToastAndroid.show('error ' + err, ToastAndroid.LONG)
            setError(error + JSON.stringify(err) + '\n')
        })
    }
    var getRecepta = () => {
        localDB.search({
            query: 'pain pita',
            fields: {
                name: 10,
                'recipeIngredient.name': -10,
                tags: 1
            },
            include_docs: true,
            highlighting: true,
            mm: 75
        })
        .then(result => {
            result.rows.forEach(element => {
                element['key'] = element.id
                element.text = element.doc.name
            })
            setReceptas(result.rows)
        })
        .catch(err => {
            ToastAndroid.show('KO ' + err.message, ToastAndroid.LONG)
        })
    }
    useEffect(() => {
        if(!receptas) {
            ToastAndroid.show('Syncing db', ToastAndroid.LONG)
            syncDb()
            getRecepta()
        }
    }, [])
    return (
    <>
        <Text>{error}</Text>
        <FlatList data={receptas}
        renderItem={({item}) => {
            return (<View style={styles.item}>
            <Text style={styles.text}>{item.text} ({item.score})</Text>
            </View>)
        }}
        />
    </>
    )
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'Roboto-Regular'
    },
    item: {
        borderBottomWidth: 1,
        padding: 15
    },
    bold: {
        fontWeight: 'bold'
    }
})

export default List