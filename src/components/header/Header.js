import React, { useRef, useState, forwardRef, useImperativeHandle } from 'react'
import {
    Animated,
    StyleSheet,
    View,
    useWindowDimensions,
    StatusBar,
    Image
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import MenuButton from './MenuButton'
import SearchBar from './SearchBar'
import { useKeyboard } from 'react-native-keyboard-height'
import { HEADER_HEIGHT, HEADER_COLOR } from '../../utils'

function Header(props, ref) {
    const windowHeight = useWindowDimensions().height
    const menuButtonRef = useRef()

    // ANIMATED PROPERTIES
    const searchWidth = useRef(new Animated.Value(1)).current
    const headerHeight = useRef(new Animated.Value(HEADER_HEIGHT)).current
    const searchBarOpacity = useRef(new Animated.Value(0)).current

    // STATES
    const [isSearchActive, setIsSearchActive] = useState(false)
    const [isSearchBarVisible, setIsSearchBarVisible] = useState(false)
    const [clickSearchButton, setClickSearchButton] = useState(false)

    // KEYBOARD SHOW/HIDE EVENTS
    const didShow = (height) => {
        if(isSearchActive) props.setGlobalSearchState(true)
    }
    const didHide = () => {
    }

    const [keyboardHeigth] = useKeyboard(didShow, didHide)
    
    var showSearchBar = () => {
        if(isSearchActive) {
            setClickSearchButton(true)
            return
        }

        props.setGlobalSearchState(true)

        menuButtonRef.current.closeMenu()
        setIsSearchActive(true)

        let viewportHeight = windowHeight - StatusBar.currentHeight
        Animated.timing(headerHeight,
            {
                toValue: viewportHeight,
                duration: 300,
                useNativeDriver: false
            }
        ).start()

        Animated.timing(searchWidth,
            {
            toValue: 100,
            duration: 200,
            delay: 20,
            useNativeDriver: false
            }
        ).start(_ => {
            setTimeout(_ => setIsSearchBarVisible(true), 1)
            Animated.timing(searchBarOpacity,
                {
                    toValue: 1,
                    duration: 500,
                    // delay: 200,
                    useNativeDriver: false
                }
            ).start()
        })
    }

    var hideSearchBar = () => {
        props.setGlobalSearchState(false)
        setIsSearchActive(false)
        setIsSearchBarVisible(false)

        Animated.timing(searchBarOpacity,
            {
                toValue: 0,
                duration: 200,
                useNativeDriver: false
            }
        ).start()

        Animated.timing(searchWidth,
            {
              toValue: 1,
              duration: 200,
              useNativeDriver: false
            }
        ).start()

        Animated.timing(headerHeight,
            {
                toValue: HEADER_HEIGHT,
                duration: 300,
                useNativeDriver: false
            }
        ).start()
    }

    useImperativeHandle(ref, _ => ({
        hideSearchBar() {
            hideSearchBar()
        },
        closeMenu() {
            menuButtonRef.current.closeMenu()
        }
    }))

    var handleClickSearch = (text) => {
        setClickSearchButton(false)
        props.drawerNav.navigate('Search', {
            query: text
        })
        hideSearchBar()
    }

    return (
        <Animated.View style={[styles.container, {
            height: headerHeight
        }]}>
            <View style={styles.menu}>
                <MenuButton ref={menuButtonRef} drawerNav={props.drawerNav} hideSearchBar={hideSearchBar} />
            </View>
            <View style={{flex: 1, flexDirection: 'row', minWidth: 0}}>
                <View style={styles.logo}>
                    <Image source={require("../../assets/images/fork.png")} style={{width: 150, height: 31}} />
                </View>
                <Animated.View style={[styles.search, {
                    flexBasis: searchWidth.interpolate({
                                inputRange: [0, 100],
                                outputRange: ['0%', '100%']
                            }),
                    height: headerHeight
                    }
                ]}>
                    <Animated.View style={{flexGrow: 1, flexShrink: 1, opacity: searchBarOpacity}}>
                        {isSearchBarVisible && <SearchBar drawerNav={props.drawerNav} hideSearchBar={hideSearchBar} clickedSearch={clickSearchButton} handleClickSearch={handleClickSearch} />}
                    </Animated.View>
                    <View style={styles.searchIconContainer}>
                        <Icon name="search" onPress={showSearchBar} size={30} />
                    </View>
                </Animated.View>
            </View>
        </Animated.View>
    )
}

const styles = StyleSheet.create({
    container: {
        minWidth: 0,
        flexDirection: 'row',
        height: HEADER_HEIGHT,

    },
    menu: {
        height: HEADER_HEIGHT,
        flexBasis: HEADER_HEIGHT,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: HEADER_COLOR
    },
    logo: {
        height: HEADER_HEIGHT,
        flexGrow: 1,
        flexShrink: 1,
        backgroundColor: HEADER_COLOR,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    search: {
        height: HEADER_HEIGHT,
        flexGrow: 0,
        minWidth: 50,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    searchIconContainer: {
        backgroundColor: HEADER_COLOR,
        flexGrow: 1,
        flexBasis: 50,
        maxWidth: 50,
        alignItems: 'center', 
        justifyContent: 'center',
        height: HEADER_HEIGHT
    }
})

export default forwardRef(Header)