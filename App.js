import React, { useEffect, useState, useLayoutEffect, useRef } from 'react';
import List from './src/components/List'
import Header from './src/components/header/Header'
import Home from './src/components/home/Home'
import CustomDrawer from './src/components/drawer/CustomDrawer'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import SplashScreen from 'react-native-splash-screen'

import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Button,
  YellowBox,
  BackHandler,
  StatusBar
} from 'react-native';
import { EventEmitter } from 'events';
import Recipe from './src/components/recipe/Recipe';
import Search from './src/components/search/Search';
import EditRecipe from './src/components/recipe/EditRecipe';
import { MenuProvider } from 'react-native-popup-menu';

YellowBox.ignoreWarnings([
    'Non-serializable values were found in the navigation state',
])

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()

function New({navigation}) {
    return (
        <Button onPress={() => navigation.navigate('List')} title="List" />
    )
}


function Settings({navigation}) {
    return (
        <>
            <Text>Causir la lenga</Text>
            <Button onPress={() => navigation.navigate('List')} title="List" />
        </>
    )
}

function DrawerMenu(props) {
    let onReceiveNav = (navObject) => {
        props.route.params.chainNavObject(navObject)
    }
    
    return (
        <Drawer.Navigator
            drawerType="slide"
            screenOptions={{
                swipeEnabled: false
            }}
            drawerContent={props => <CustomDrawer {...props} chainNavObject={(navObject) => onReceiveNav(navObject)} />}
            initialRouteName="List"
        >
            <Drawer.Screen name="Home" component={Home}
            listeners={{
                drawerClose: e => {
                    props.route.params.handleDrawerClose()
                }
            }} />
            <Drawer.Screen name="List" component={List}
            listeners={{
                drawerClose: e => {
                    props.route.params.handleDrawerClose()
                }
            }} />
            <Drawer.Screen name="Recipe" component={Recipe}
            listeners={{
                drawerClose: e => {
                    props.route.params.handleDrawerClose()
                }
            }} />
            <Drawer.Screen name="EditRecipe" component={EditRecipe}
            listeners={{
                drawerClose: e => {
                    props.route.params.handleDrawerClose()
                }
            }} />
            <Drawer.Screen name="Search" component={Search}
            listeners={{
                drawerClose: e => {
                    props.route.params.handleDrawerClose()
                }
            }} />
        </Drawer.Navigator>
    )
}

/************************/
/* MAIN STACK (Bandeau) */
/************************/

function MainStack() {
    const headerRef = useRef()
    const [drawerNav, setDrawerNav] = useState(null)
    var onReceiveNav = (navObject) => {
        setDrawerNav(navObject)
    }
    
    // Handle back button for search bar
    const [isSearchBarOpen, setIsSearchBarOpen] = useState(false)

    useLayoutEffect(
        React.useCallback(() => {
            const onBackpress = () => {
                if(!isSearchBarOpen) {
                    return false
                } else {
                    headerRef.current.hideSearchBar()
                    setIsSearchBarOpen(false)
                    return true
                }
            }
            BackHandler.addEventListener('hardwareBackPress', onBackpress)
            return () => {
                BackHandler.removeEventListener('hardwareBackPress', onBackpress)
            }
        }, [isSearchBarOpen, setIsSearchBarOpen])
    )

    const handleDrawerClose = _ => {
        headerRef.current.closeMenu()
    }

    return (
        <Stack.Navigator
        screenOptions={({ navigation }) => ({
            header: ({ navigation }) => <Header ref={headerRef} drawerNav={drawerNav} setGlobalSearchState={(e) => setIsSearchBarOpen(e)} />
        })}>
            <Stack.Screen name="DrawerMenu" component={DrawerMenu} initialParams={{chainNavObject: (navObject) => onReceiveNav(navObject), handleDrawerClose}} />
        </Stack.Navigator>
    )
}

function App() {
    useEffect(() => {
        SplashScreen.hide()
    }, [])
    return (
        <MenuProvider>
            <StatusBar backgroundColor='#daa700' />
            <NavigationContainer>
                <MainStack />
            </NavigationContainer>
        </MenuProvider>
    )
}

const styles = StyleSheet.create({
    text: {
        color: 'blue'
    },
    item: {
        borderBottomWidth: 1,
        padding: 15
    },
    view: {
        borderColor: 'green',
        borderWidth: 2
    }
})

export default App