import React, { useState, useEffect, useLayoutEffect, useRef } from 'react'
import { ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, BackHandler, StatusBar, Alert, View, KeyboardAvoidingView } from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker';
import { useFocusEffect } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import EditIngredient from './EditIngredient';
import PouchDB from '../../pouchdb'
import { localDB, remoteDB } from '../../utils'
import { MenuTrigger, MenuOptions, MenuOption, Menu } from 'react-native-popup-menu'
import { useKeyboard } from 'react-native-keyboard-height';

var handlerSync = null

function EditRecipe({route, navigation}) {
    const container = useRef(null)
    const [paddingBottom, setPaddingBottom] = useState(0)
    // Prep time
    const [prepTime, setPrepTime] = useState(new Date((new Date()).setHours(0, 0, 0)))
    const [showPrepTimePicker, setShowPrepTimePicker] = useState(false)
    // Cook time
    const [cookTime, setCookTime] = useState(new Date((new Date()).setHours(0, 0, 0)))
    const [showCookTimePicker, setShowCookTimePicker] = useState(false)
    // EditIngredients
    const [ingredientEditors, setIngredientEditors] = useState([])
    const [addedIngredientEditor, setAddedIngredientEditor] = useState(false)
    // Units and ingredients lists
    const [units, setUnits] = useState(null)
    const [ingredients, setIngredients] = useState(null)
    
    // KEYBOARD SHOW/HIDE EVENTS
    const didShow = (height) => {
        console.log(height)
        setPaddingBottom(height)
    }
    const didHide = () => {
        setPaddingBottom(0)
    }

    const [keyboardHeigth] = useKeyboard(didShow, didHide)
    
    const syncDb = () => {
        handlerSync = PouchDB.sync(remoteDB, localDB, {
            live: true,
            retry: true,
        })
        .on('change', (info) => {})
        .on('paused', (err) => {})
        .on('active', () => {})
        .on('denied', (err) => {})
        .on('complete', (info) => {})
        .on('error', (err) => {})
    }

    const getUnits = () => {
        localDB.query('units_index', {group: true}).then(function (result) {
            result.rows = result.rows.filter(u => u.value > 10)
            result.rows.sort((a, b) => a.value < b.value)
            setUnits(result.rows.map(r => r.key))
        }).catch(function (err) {
            console.log(err)
        })
    }

    const getIngredients = () => {
        localDB.query('ingredients_index', {group: true}).then(function (result) {
            setIngredients(result.rows.map(r => r.key))
        }).catch(function (err) {
            console.log(err)
        })
    }

    const onChange = (event, selectedTime, updateFunction, showFunction) => {
        showFunction(false)
        if(selectedTime !== undefined) {
            updateFunction(selectedTime)
        }
    }
    const showPicker = (showFunction) => {
        showFunction(true)
    }

    useFocusEffect(
        React.useCallback(() => {
            // BACK HANDLER
            const onBackpress = () => {
                close()
                return true
            }
            BackHandler.addEventListener('hardwareBackPress', onBackpress)
            // STATUS BAR
            StatusBar.setHidden(true)
            // HEADER
            let stack = navigation.dangerouslyGetParent()
            if(stack) stack.setOptions({headerShown: false})
            return () => {
                BackHandler.removeEventListener('hardwareBackPress', onBackpress)
                StatusBar.setHidden(false)
                setIngredientEditors([])
            }
        }, [navigation])
    )

    const close = () => {
        const decide = new Promise((resolve, reject) => {
            Alert.alert('Êtes-vous sûr de vouloir partir ?', 'Les modifications apportées à la recette seront perdues.', [
                {
                    text: 'Rester',
                    onPress: () => resolve(false)
                },
                {
                    text: 'Partir',
                    onPress: () => resolve(true)
                }
            ],
            {
                cancelable: false
            })
        })
        decide.then(leave => {
            if(leave) {
                goBack()
            }
        })
    }

    const goBack = () => {
        let stack = navigation.dangerouslyGetParent()
        if(stack) stack.setOptions({headerShown: true})
        setTimeout(() => {
            navigation.goBack()
        }, 100);
    }

    const addIngredientEditor = () => {
        let i = [...ingredientEditors]
        i.push(<EditIngredient key={i.length} ingredients={ingredients} units={units} />)
        setIngredientEditors(i)
        // setAddedIngredientEditor(true)
    }

    useEffect(() => {
        if(!units && !ingredients) {
            syncDb()
            getUnits()
            getIngredients()
        }
    })

    return (
        <ScrollView ref={container}>
            <TouchableOpacity style={styles.close} onPress={close}>
                <Icon name="times" size={25} color="#cacaca" />
            </TouchableOpacity>
            {/* TITLE */}
            <Text>Títol</Text>
            <TextInput
            style={styles.input}
            defaultValue={route.params?.recipe?.name} />
            
            {/* DESCRIPTION */}
            <Text>Descripcion (opcional)</Text>
            <TextInput
            multiline={true}
            numberOfLines={4}
            textAlignVertical="top"
            style={styles.input}
            defaultValue={route.params?.recipe?.description} />
            
            {/* YIELD */}
            <Text>Nombre de personas</Text>
            <TextInput
            style={[styles.input, styles.yield]}
            textAlign="center"
            keyboardType="number-pad"
            defaultValue={route.params?.recipe?.recipeYield.toString()} />

            {/* PREP TIME */}
            <Text>Temps de preparacion</Text>
            <TouchableOpacity onPress={() => {
                showPicker(setShowPrepTimePicker)
            }}>
                <Text style={styles.time}>{prepTime.getHours().toString().padStart(2, '0') + ':' + prepTime.getMinutes().toString().padStart(2, 0)}</Text>
            </TouchableOpacity>
            {showPrepTimePicker &&
                <DateTimePicker
                mode="time"
                value={prepTime}
                display="spinner"
                is24Hour={true}
                onChange={(event, selectedTime) => {
                    onChange(event, selectedTime, setPrepTime, setShowPrepTimePicker)
                }}
                minuteInterval={5}
            />}

            {/* COOK TIME */}
            <Text>Temps de coseson</Text>
            <TouchableOpacity onPress={() => {
                showPicker(setShowCookTimePicker)
            }}>
                <Text style={styles.time}>{cookTime.getHours().toString().padStart(2, '0') + ':' + cookTime.getMinutes().toString().padStart(2, 0)}</Text>
            </TouchableOpacity>
            {showCookTimePicker &&
                <DateTimePicker
                mode="time"
                value={prepTime}
                display="spinner"
                is24Hour={true}
                onChange={(event, selectedTime) => {
                    onChange(event, selectedTime, setCookTime, setShowCookTimePicker)
                }}
                minuteInterval={5}
                />
            }

            {/* INGREDIENTS */}
            <View style={styles.ingredientsTile}>
                <Text style={styles.header}>Ingredients</Text>
                <Menu style={{justifyContent: 'center'}}>
                    <MenuTrigger>
                        <View style={{width: 20, justifyContent: 'center', alignItems: 'center'}}>
                            <Icon name="ellipsis-v" size={17} />
                        </View>
                    </MenuTrigger>
                    <MenuOptions>
                        <MenuOption text='Apondre una seccion' />
                    </MenuOptions>
                </Menu>
            </View>
            <TouchableOpacity onPress={addIngredientEditor} style={styles.addIngredient}>
                <Text>Apondre un ingredient</Text>
                <Icon name="plus" size={15} style={styles.plusIcon} />
            </TouchableOpacity>
            <View>{ingredientEditors}</View>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieeeeeu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <Text>Adieu</Text>
            <View style={{height: paddingBottom}} />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    header: {
        fontSize: 20
    },
    input: {
        borderWidth: 1
    },
    yield: {
        width: 50
    },
    time: {
        fontSize: 20
    },
    close: {
        alignItems: 'flex-end',
        margin: 10
    },
    addIngredient: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    plusIcon: {
        marginLeft: 10
    },
    ingredientsTile: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        paddingHorizontal: 20
    }
})

export default EditRecipe