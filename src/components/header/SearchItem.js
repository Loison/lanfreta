import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { NavigationActions } from 'react-navigation'

function SearchItem({drawerNav, title, id, hideSearchBar}) {
    var goToRecipe = () => {
        let {index, routes} = drawerNav.dangerouslyGetState()
        let routeName = routes[index].name
        if(routeName == "Recipe") {
            drawerNav.setParams({id})
        } else {
            drawerNav.navigate('Recipe', {id})
        }
        setTimeout(_ => hideSearchBar(), 100)
    }
    return (
        <TouchableOpacity onPress={goToRecipe} style={styles.button}>
            <Text>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        padding: 10
    }
})

export default SearchItem