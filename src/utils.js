import PouchDB from './pouchdb'

export const HEADER_HEIGHT = 50
export const HEADER_COLOR = '#efbb00'

export function recipeNameToID(title) {
    let noAccents = title.normalize("NFD").replace(/[\u0300-\u036f]/g, '')
    noAccents = noAccents.toLowerCase().replace(/\W+/g, ' ').replace('œ', 'oe').trim()
    return noAccents.replace(/ /g, '-')
}

const url = 'api.milonelois.com'

export const remoteDB = new PouchDB(`https://${url}/receptas`)
export const localDB = new PouchDB('receptas', {adapter: 'react-native-sqlite'})

export const removeFromArray = (arr, ...a) => {
    var what, L = a.length, i, cp = [...arr]
    while (L > 0 && cp.length) {
        what = a[--L]
        while ((i = cp.indexOf(what)) !== -1) {
            cp.splice(i, 1)
        }
    }
    return cp
}