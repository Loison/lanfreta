var http = require('http')
var https = require('https')
var nodeHtmlParser = require("node-html-parser")
var fs = require('fs')

const UNITS = ["bol","grand bol","tasse","petit pot","pot","pots","sachet","sachets","bouquet","bouquets","c à c","c. à café","c. à café bombées","cuillère à café","cuillères à café","c à s","c. à soupe","cuillère à soupe","cuillères à soupe","g","kg","ml","cl","l","litre","pincée","pincées","poignée","grosse poignée","petite poignée","filet","boîte","boîtes","gousse","gousses","branche","branches","morceau","petit morceau","morceaux","milliards","botte","bottes","goutte","gouttes","dose","doses","bâton","bâtons","bouteille","tige","carcasse","bulbe","tranche","feuille","feuilles","brin","brins"]
const FIELDS = ["@context", "@type", "name", "image", "datePublished", "prepTime", "cookTime", "totalTime", "recipeYield", "recipeIngredient", "RecipeInstruction", "author", "description", "keywords", "recipeCuisine", "tags"]

function getPromise(url) {
    return new Promise((resolve, reject) => {
        let protocol = url.charAt(4) == 's' ? https : http
        protocol.get(url, (res) => {
            var rawData = ''
            res.on('data', (chunk) => { rawData += chunk })
            res.on('end', () => {
                var root = nodeHtmlParser.parse(rawData, {script: true})
                var scripts = root.querySelectorAll('script')
                for (let script of scripts) {
                    try {
                        if(script.childNodes.length == 0) continue
                        let scriptText = script.childNodes[0].rawText
                        if(!/Recipe/.test(scriptText)) continue
                        let parsedData = JSON.parse(scriptText)
                        if(Array.isArray(parsedData)) {
                            parsedData = parsedData.filter((i) => i['@type'] == "Recipe")[0]
                        }
                        if(parsedData['@type'] && parsedData['@type'] == "Recipe") {
                            resolve(parsedData)
                        } else {
                            continue
                        }
                    } catch (e) {
                        continue
                    }
                }
                reject('No recipe found')
            })
        }).on('error', function(e) {
            reject(e.message)
        })
    })
}

module.exports = {
    async getRecipe(url) {
        try {
            let recipe = await getPromise(url)
            return recipe
        } catch(e) {
            console.log(e)
            return false
        }
    },
    parseIngredient(ingredient) {
        let number = '\\d+(?:(?:\.|,)\\d+)?'
        let r = new RegExp('(' + number + ') ?(' + UNITS.join('|') + ') (?:d\'|de)? ?(.+)$')
        qt = null
        unit = null,
        name = null
        if(r.test(ingredient)) {
            [, qt, unit, name] = ingredient.match(r)
        // } else if(new RegExp(number + ' d(?:e|\') ?.*').test(ingredient)) {
        //     [, qt, name] = ingredient.match(new RegExp('(' + number + ') (?:de|d\')? ?(.*)'))
        } else if(new RegExp(number + ' .*').test(ingredient)) {
            [, qt, name] = ingredient.match(new RegExp('(' + number + ') (.*)'))
        } else {
            name = ingredient
        }
        return {
            qt: !isNaN(qt) ? qt : null,
            unit: unit ? unit.replace(/(\S)s(\W|$)/g, '$1$2') : null,
            name: name.replace(/ *\([^)]*\) */g, '')
        }
    },
    parseInstructions(instructions) {
        if(instructions.every(i => typeof i === "object" && i["@type"] == "HowToStep")) {
            return instructions
        } else if(instructions.every(i => typeof i === "string")) {
            return instructions.map(i => ({"@type": "HowToStep", "text": i}))
        } else {
            return instructions.split('\n').map(i => ({"@type": "HowToStep", "text": i}))
        }
    },
    parseYield(raw) {
        if(/\d+ .*/i.test(raw)) {
            [yield_text, yield_nb, yield_unit] = raw.match(/(\d+) (.*)/)
            if(yield_unit.includes('personne')) {
                return yield_nb
            } else {
                return {
                    "nb": yield_nb,
                    "unit": yield_unit.replace(/s$/, ''),
                    "raw": yield_text
                }
            }
        } else {
            return {
                "nb": 1,
                "unit": "plat",
                "raw": "1 plat"
            }
        }
    },
    titleToID(title) {
        let noAccents = title.normalize("NFD").replace(/[\u0300-\u036f]/g, '')
        noAccents = noAccents.toLowerCase().replace(/\W+/g, ' ').replace(/œ/i, 'oe').trim()
        return noAccents.replace(/ /g, '-')
    },
    parseRecipe(recipe) {
        recipe['_id'] = module.exports.titleToID(recipe.name)
        // Ingredients
        let ingredient = []
        for(let i of recipe.recipeIngredient) {
            ingredient.push(module.exports.parseIngredient(i))
        }
        recipe.recipeIngredient = ingredient
        recipe.recipeInstructions = module.exports.parseInstructions(recipe.recipeInstructions)
        recipe.recipeYield = module.exports.parseYield(recipe.recipeYield)
        for(let o of Object.keys(recipe)) {
            if(!FIELDS.includes(o)) delete recipe[o]
        }
        return recipe
    }
}

