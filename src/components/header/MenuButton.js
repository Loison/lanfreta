import React, { useRef, useState, useEffect, forwardRef, useImperativeHandle } from 'react'
import {
    Animated,
    StyleSheet,
    TouchableWithoutFeedback
} from 'react-native'

function menuButton(props, ref) {
    const [isOpenMenu, setIsOpenMenu] = useState(false)
    const spinForkAnim = useRef(new Animated.Value(0)).current
    const knifeMarginTop = useRef(new Animated.Value(5)).current
    const spoonOpacity = useRef(new Animated.Value(1)).current

    var openMenuAnimation = () => {
        Animated.spring(spinForkAnim, {
            toValue: 1,
            useNativeDriver: false
        }).start()
        Animated.spring(knifeMarginTop, {
            toValue: -21,
            useNativeDriver: false
        }).start()
        Animated.spring(spoonOpacity, {
            toValue: 0,
            useNativeDriver: false
        }).start()
        setIsOpenMenu(true)
    }
    var closeMenuAnimation = () => {
        Animated.spring(spinForkAnim, {
            toValue: 0,
            useNativeDriver: false
        }).start()
        Animated.spring(knifeMarginTop, {
            toValue: 5,
            useNativeDriver: false
        }).start()
        Animated.spring(spoonOpacity, {
            toValue: 1,
            useNativeDriver: false
        }).start()
    }

    var openMenu = () => {
        if(props.drawerNav) {
            props.hideSearchBar()
            openMenuAnimation()
            props.drawerNav.openDrawer()
        }
    }
    
    useImperativeHandle(ref, _ => ({
        closeMenu() {
            closeMenu()
        }
    }))
    var closeMenu = () => {
        if(!isOpenMenu) return
        setIsOpenMenu(false)
        if(props.drawerNav) {
            closeMenuAnimation()
            props.drawerNav.closeDrawer()
        }
    }

    return (
        <TouchableWithoutFeedback onPress={() => {
            isOpenMenu ? closeMenu() : openMenu()
        }}>
            <Animated.View style={styles.container}>
                <Animated.Image style={[styles.fork, {transform: [
                                {
                                    rotate: spinForkAnim.interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [
                                            '0deg', '-45deg'
                                        ],
                                    })
                                }
                            ]}]}
                source={require('../../assets/images/fork.png')} />
                <Animated.Image style={[styles.spoon, {opacity: spoonOpacity}]} />
                <Animated.Image style={[styles.knife, {
                                transform: [{
                                    rotate: spinForkAnim.interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [
                                            '180deg', '45deg'
                                        ],
                                    })
                                }],
                                marginTop: knifeMarginTop
                            }]} />
            </Animated.View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 35,
        height: 35
    },
    fork: {
        height: 8,
        width: 35
    },
    spoon: {
        height: 8,
        width: 35,
        marginTop: 5,
        backgroundColor: 'blue'
    },
    knife: {
        height: 8,
        width: 35,
        backgroundColor: 'blue'
    }
})

menuButton = forwardRef(menuButton)

export default menuButton