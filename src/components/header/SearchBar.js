import React, { useEffect, useState } from 'react'
import { TextInput } from 'react-native-gesture-handler'
import { Text, StyleSheet, View, FlatList, useWindowDimensions } from 'react-native'
import { recipeNameToID } from '../../utils'
import PouchDB from '../../pouchdb'
import { remoteDB, localDB, HEADER_HEIGHT } from '../../utils'
import SearchItem from './SearchItem'

let handlerSync = null

function ItemSeparator() {
    return (
        <View style={styles.separator}></View>
    )
}

function searchBar(props) {
    const windowHeight = useWindowDimensions().height
    const [resultList, setResultList] = useState([])
    const [searchText, setSearchText] = useState('')
    const [refreshing, setRefreshing] = useState(false)

    useEffect(() => {
        if(props.clickedSearch) props.handleClickSearch(searchText)
        if(!resultList) syncDb()
        return () => {
            if(handlerSync !== null) handlerSync.cancel()
        }
    }, [props.clickedSearch])

    var handleTextChange = text => {
        setSearchText(text)
        runSearchQuery(recipeNameToID(text))
    }

    var syncDb = () => {
        handlerSync = PouchDB.sync(remoteDB, localDB, {
            live: true,
            retry: true,
        })
            .on('change', (info) => {})
            .on('paused', (err) => {})
            .on('active', () => {})
            .on('denied', (err) => {})
            .on('complete', (info) => {})
            .on('error', (err) => {})
    }
    var runSearchQuery = (text) => {
        if(text == "") {
            setResultList([])
            return
        }

        let options = {
            startkey: text,
            endkey: text + "\ufff0",
            limit: 15
        }

        localDB.query('name_index', options)
        .then(result => {
            // remove duplicates
            result.rows = result.rows.filter((value, index, rows) =>
                index === rows.findIndex(e => e.id === value.id)
            )
            setResultList(result.rows)
        })
        .catch(err => {
            console.log('error:', err)
        }).finally(_ => setRefreshing(false))
    }
    return (
        <View style={styles.container}>
            <TextInput placeholder="Cercar una recèpta" autoFocus={true} style={styles.input} onChangeText={text => handleTextChange(text)} clearButtonMode="always" onSubmitEditing={e => props.handleClickSearch(searchText)} />
            <View style={[styles.list, {maxHeight: windowHeight - HEADER_HEIGHT - 50}]}>
                <FlatList
                data={resultList}
                // extraData={resultList}
                keyExtractor={i => i.id}
                renderItem={({item}) => <SearchItem title={item.value} id={item.id} drawerNav={props.drawerNav} hideSearchBar={props.hideSearchBar} />}
                ItemSeparatorComponent={ItemSeparator}
                ListEmptyComponent={<Text>Pas res !</Text>}
                ListHeaderComponent={<Text>Cercatz amics, cercatz !</Text>}
                keyboardShouldPersistTaps='handled' />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    input: {
        flexBasis: 50,
        paddingLeft: 10
    },
    list: {
    },
    separator: {
        width: '100%',
        height: 1,
        backgroundColor: 'darkgrey'
    }
})

export default searchBar