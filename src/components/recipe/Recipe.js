import React, { useState, useEffect } from 'react'
import { Text, Button, StyleSheet, Image, View, useWindowDimensions, NativeModules, ToastAndroid } from 'react-native'
import PouchDB from '../../pouchdb'
import { remoteDB, localDB } from '../../utils'
import { ScrollView } from 'react-native'
import RNShake from 'react-native-shake'

const { RNImageSize } = NativeModules

var handlerSync = null
const baseImageUrl = 'https://cosina.milonelois.com/?file='

function Recipe({route, navigation}) {
    const [recipe, setRecipe] = useState(null)
    const [imageUrl, setImageUrl] = useState(null)
    const windowWidth = useWindowDimensions().width;
    const windowHeight = useWindowDimensions().height;
    const [imageDimensions, setImageDimensions] = useState({width: windowWidth, height: 100})

    var syncDb = () => {
        handlerSync = PouchDB.sync(remoteDB, localDB, {
            live: true,
            retry: true,
        })
            .on('change', (info) => {
            })
            .on('paused', (err) => {
            })
            .on('active', () => {
            })
            .on('denied', (err) => {
            })
            .on('complete', (info) => {
            })
            .on('error', (err) => {
            })
    }
    var getRecipe = () => {
        localDB.get(route.params.id)
        .then(result => {
            setRecipe(result)
            let url;
            // Get image URI
            if(result.image?.substring(0, 4) == 'http') {
                url = result.image
            } else if(result.image) {
                url = baseImageUrl + result.image
            }
            setImageUrl(url)
            console.log('URL', imageUrl)
            if(url == '') return
            // Get Image size
            Image.getSize(url, (width, height) => {
                // calculate image width and height 
                const screenWidth = windowWidth
                const scaleFactor = width / screenWidth
                const imageHeight = height / scaleFactor
                setImageDimensions({...imageDimensions, height: imageHeight})
                console.log('Image dimensions set !')
            }, err => console.log(err))
        })
        .catch(err => {
            console.log('KO', err.message)
        })
    }
    const editRecipe = () => {
        if(recipe) {
            navigation.navigate('EditRecipe', {recipe})
        }
    }
    useEffect(() => {
        console.log('effect')
        syncDb()
        getRecipe()
        RNShake.addEventListener('ShakeEvent', () => {
            editRecipe()
        })
        return () => {
            RNShake.removeEventListener('ShakeEvent')
        }
    }, [route.params.id])

    if(recipe) {
        return (
            <ScrollView style={styles.container}>
                <Button onPress={editRecipe} title="modificar" />
                <Text style={styles.title}>{recipe.name}</Text>
                <Image
                source={imageUrl ? {
                    uri: imageUrl,
                    height: imageDimensions.height,
                    width: imageDimensions.width
                } : null}
                defaultSource={require('../../assets/images/placeholder.jpg')}
                style={[{height: imageDimensions.height}, styles.image]} />
                <Text>Pour {typeof recipe.recipeYield == "object" ? recipe.recipeYield.raw : recipe.recipeYield}</Text>
                <Text>{recipe.totalTime}</Text>
                <Text></Text>
                <Text selectable={true}>{recipe.recipeIngredient.map(i => (i.qt ? i.qt : '') + ' ' + (i.unit ? i.unit : '') + ' ' + i.name).join('\n')}</Text>
                <Text></Text>
                <Text selectable={true}>{recipe.recipeInstructions.map((val, i) => i + 1 + ' ' + val.text).join('\n\n')}</Text>
            </ScrollView>
        )
    } else {
        return (
            <Text>Loading...</Text>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Roboto',
        flex: 1
    },
    title: {
        fontFamily: 'Roboto-Medium',
        fontSize: 25,
        padding: 20,
        alignSelf: 'center',
        textAlign: 'center'
    }
})

export default Recipe