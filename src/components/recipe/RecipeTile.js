import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'

const baseImageUrl = 'https://cosina.milonelois.com/?file='

function RecipeTile(props) {
    const [imageUrl, setImageUrl] = useState(null)
    useEffect(() => {
        if(props.recipe.image?.substring(0, 4) == 'http') {
            setImageUrl(props.recipe.image)
        } else if(props.recipe.image) {
            setImageUrl(baseImageUrl + props.recipe.image)
        }
    }, [])
    return (
        <TouchableOpacity onPress={e => props.onPress(props.recipe._id)}>
            <View style={styles.tile}>
                <Image
                source={imageUrl ? {uri: imageUrl} : null}
                defaultSource={require('../../assets/images/placeholder.jpg')}
                style={styles.image} />
                <Text>{props.recipe.text ? props.recipe.text : props.recipe.name}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    tile: {
        borderColor: 'lightgrey',
        borderWidth: 1,
        marginBottom: 15,
        marginHorizontal: 15,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    image: {
        width: 80,
        height: 80,
        marginRight: 10
    }
})

export default RecipeTile